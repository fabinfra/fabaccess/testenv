# API-Testsetup, with Docker

# NOTE: This repo is 98% identical to https://gitlab.com/fabinfra/fabaccess/demos-environments/dockercompose and is therefore going to be archived (21.02.2025, vmario891)

only for API testing. ATM implements: machines::* & machine::read, authenticate

* run `docker-compose up` in this directory

# Users:
* Admin - Access to everything
* Manager - Manage Permission to Machines with same Letter (A, B, C)
* Maker - Use Permission to Machines with same Letter (A, B, C)
* Guest - Disclose Permission only for all Machines

## Password
Password for all Users is: `secret`

## List of Users
```
Admin1
Admin2

ManagerA1
ManagerA2
ManagerB1
ManagerB2
ManagerC1
ManagerC2
ManagerABC1
ManagerABC2

MakerA1
MakerA2
MakerB1
MakerB2
MakerC1
MakerC2
MakerACB1
MakerACB2
MakerACB3

GuestA1
GuestA2
GuestB1
GuestB2
GuestC1
GuestC2
GuestACB1
GuestACB2

MakerQRA
MakerQRB
MakerQRC
```

# Machines
Machines have all a Dummy Actor

## List of Categories
```
CategoryA
CategoryB
CategoryC
```

## List of Machines
```
MachineA1
MachineA2
MachineA3
MachineA4
MachineA5

MachineB1
MachineB2
MachineB3
MachineB4
MachineB5

MachineC1
MachineC2
MachineC3
MachineC4
MachineC5
```

# Roles
All Roles have only one Permission
Users have multipile Roles to give them access
`TestEnv.Admin` have all Permissions

## List of Roles
```
Admin

ManageA
ManageB
ManageC

UseA
UseB
UseC

ReadA
ReadB
ReadC

DiscloseA
DiscloseB
DiscloseC
```

## List of Permissions
```
TestEnv.Admin

TestEnv.Manage.A
TestEnv.Manage.B
TestEnv.Manage.C

TestEnv.Write.A
TestEnv.Write.B
TestEnv.Write.C

TestEnv.Read.A
TestEnv.Read.B
TestEnv.Read.C

TestEnv.Disclose.A
TestEnv.Disclose.B
TestEnv.Disclose.C
```
